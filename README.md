# CLM reto desarrollo

## Servicio desarrollado con KOA js

### modo de ejecución

- Clone el repositorio
- Dirijase a la carpeta del proyecto
- Ejecute el comando docker-compose up para levantar los servicios

Se levantaran 3 servicios

- Base de datos mongodb en: `localhost:271017`
- Interfaz web mongo-express `localhost:8081`
- Api Rest buscador de peliculas en `http://localhost:8080/swagger`
