const mongoose = require('mongoose');

const schema = new mongoose.Schema(
  {
    title: String,
    year: String,
    released: String,
    genre: String,
    director: String,
    actors: String,
    plot: String,
    ratings: [{}],
  },
  { timestamps: true }
);

module.exports = mongoose.model('Movie', schema, 'movies');
