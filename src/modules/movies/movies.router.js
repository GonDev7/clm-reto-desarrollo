const MovieController = require('./movies.controller');

module.exports = (router) => {
  router
    .get('/movies', MovieController.list)
    .get('/movie/:title', MovieController.byTitle)
    .post('/movie', MovieController.create)
    .put('/movie', MovieController.update);
};
