const MovieModel = require('./movies.model');

module.exports = class Movie {
  static list = async (ctx) => {
    await MovieModel.find().exec().then(ctx.ok, ctx.badRequest);
  };

  static byTitle = async (ctx) => {
    const title = ctx.params.title;
    await MovieModel.findOne({ title: { $regex: title, $options: 'i' } })
      .exec()
      .then(ctx.ok, ctx.badRequest);
  };

  static create = async (ctx) => {
    const title = ctx.request.body.title;
    const movieFind = await MovieModel.findOne({
      title: { $regex: title, $options: 'i' },
    })
      .exec()
      .then(ctx.ok, ctx.badRequest);

    return !movieFind.response.body
      ? await MovieModel.create(ctx.request.body).then(ctx.ok, ctx.badRequest)
      : (ctx.body = { messagge: 'Pélicula ya registrada' });
  };

  static update = async (ctx) => {
    const find = ctx.request.body.find;
    const replace = ctx.request.body.replace;
    const movie = ctx.request.body.movie;

    const movieFind = await MovieModel.findOne({
      title: { $regex: movie, $options: 'i' },
    })
      .exec()
      .then(ctx.ok, ctx.badRequest);

    if (!movieFind.response.body) {
      return (ctx.body = { messagge: 'Pélicula no existe' });
    } else {
      const plot = movieFind.response.body.plot;
      const newPlot = plot.includes(find)
        ? plot.replace(new RegExp(find, 'gi'), replace)
        : plot;
      await MovieModel.updateOne({ plot: newPlot });
      return ctx.ok(
        (ctx.body = {
          messagge: 'Plot modificado con éxito',
          new_plot: {
            plot: newPlot,
          },
        })
      );
    }
  };
};
