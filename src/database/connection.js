const mongoose = require('mongoose');

mongoose
  .connect('mongodb://root:root@mongo:27017', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then((db) =>
    console.log(
      `DB in conected to ${db.connection.host}:${db.connection.port} in Database ${db.connection.name}`
    )
  )
  .catch((err) => console.error(err));
