const Koa = require('koa');
const swagger = require('swagger2');
const Router = require('koa-router');
const { ui } = require('swagger2-koa');
const respond = require('koa-respond');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const swaggerDocument = swagger.loadDocumentSync('api.yaml');
const app = new Koa();
const router = new Router();
require('./modules/movies/movies.router')(router);
require('./database/connection');
require('colors');

app
  .use(ui(swaggerDocument, '/swagger'))
  .use(respond())
  .use(cors())
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(PORT, HOST, () => {
  console.log(`Server running on http://${HOST}:${PORT}`.blue);
});
